import React, {useState} from 'react'

const Introduction = () => {

  const [dark, setDark] = useState()

  const styleDarkTheme = {
    backgroundColor: dark ? 'black' : 'white',
    color: dark ? 'white' : 'black',
  }

  const darkTheme = () => {
    document.getElementById("dark").disabled = true;
    document.getElementById("white").disabled = false;
  }

  const whiteTheme = () => {
    document.getElementById("white").disabled = true;
    document.getElementById("dark").disabled = false;
  }

  return (
      <div className='content' style={styleDarkTheme}>
        <div className='btn'>
          <button 
          style={{backgroundColor: 'black', color: 'white', cursor: 'pointer'}} 
          id='dark' onClick={() => darkTheme(setDark(prevTheme => !prevTheme))}>
            Dark Mode &#127761;
            </button>
          <button style={{backgroundColor: 'white', cursor: 'pointer'}} 
          id='white' onClick={() => whiteTheme(setDark(prevTheme => !prevTheme))}>
            White Mode &#9728;&#65039;
          </button>
        </div>

        <h1 style={styleDarkTheme}>Introduction</h1>
        <p style={styleDarkTheme}>
        ReactJS is a declarative, efficient, and flexible JavaScript library for building user interfaces. 
        It is an open-source, component-based front-end library that is responsible only for the view layer of the application.
        ReactJS is not a framework, it is just a library developed by Facebook to solve some problems that we were facing earlier.
        </p>
        <h1 style={styleDarkTheme}>React JS Features</h1>
        <p style={styleDarkTheme}>
        <ul>
            <li style={styleDarkTheme}>
            Use JSX: JSX is faster than normal JavaScript as it performs optimizations while translating to regular JavaScript. 
            It makes it easier for us to create templates.
            </li>
            <br></br>
            <li style={styleDarkTheme}>
            Component: A Component is one of the core building blocks of React. 
            In other words, we can say that every application you will develop in React will be made up of pieces called components. 
            Components make the task of building UIs much easier.
            </li>
        </ul>
        </p>
        <h1 style={styleDarkTheme}>React JS Advantages</h1>
        <p style={styleDarkTheme}>
        <ul>
            <li style={styleDarkTheme}>
            Composable: We can divide these codes and put them in custom components. 
            Then we can utilize those components and integrate them into one place.
            </li>
            <br></br>
            <li style={styleDarkTheme}>
            Declarative: In react the DOM is declarative. 
            We can make interactive UIs by changing the state of the component and React takes care of updating the DOM according to it.
            </li>
            <br></br>
            <li style={styleDarkTheme}>
            Easy to learn: HTML-like syntax of JSX make you comfortable with the codes of React, it only requires to need a basic knowledge of HTML, CSS, and JS fundamentals to start working with it.
            </li>
        </ul>
        </p>
      </div>
  )
}

export default Introduction