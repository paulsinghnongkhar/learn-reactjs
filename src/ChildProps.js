import React from 'react'

function ChildProps(props) {
  return (
    <div className='child-component'>
      <button onClick={() => props.greetHandler('Child Props')}>Greet Child</button>
    </div>
  )
}

export default ChildProps