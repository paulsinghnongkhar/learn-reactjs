import React, {useState} from 'react'
import SyntaxHighlighter from 'react-syntax-highlighter';

function EventHandling() {
  const codeString = 
`import React from 'react'

function EventHandling() {
    function clickHandler() {
        document.getElementById("message").innerHTML = "Thank you";
    }
  return (
    <div>
        <h1 id='message'></h1>
      <button onClick={clickHandler}>Click me to display message</button>
    </div>
  )
}

export default EventHandling`;
    
const [dark, setDark] = useState()

const styleDarkTheme = {
  backgroundColor: dark ? 'black' : 'white',
  color: dark ? 'white' : 'black',
}

const darkTheme = () => {
  document.getElementById("dark").disabled = true;
  document.getElementById("white").disabled = false;
}

const whiteTheme = () => {
  document.getElementById("white").disabled = true;
  document.getElementById("dark").disabled = false;
}

    function clickHandler() {
        document.getElementById("message").innerHTML = "Thank you";
    }
  return (
    <div className='content' style={styleDarkTheme}>
        <div className='btn'>
          <button style={{backgroundColor: 'black', color: 'white'}} id='dark' onClick={() => darkTheme(setDark(prevTheme => !prevTheme))}>
            Dark Mode &#127761;
            </button>
          <button style={{backgroundColor: 'white'}} id='white' onClick={() => whiteTheme(setDark(prevTheme => !prevTheme))}>
            White Mode &#9728;&#65039;
          </button>
        </div>
      <h1 style={styleDarkTheme}>Event Handling</h1>
      <p style={styleDarkTheme}>Here is the code below &#11015;&#65039;</p>
      <SyntaxHighlighter language="javascript" showLineNumbers={true}>
        {codeString}
      </SyntaxHighlighter>
      <p style={styleDarkTheme}>Here is the output below &#11015;&#65039;</p> 
        <h1 id='message' style={styleDarkTheme}></h1>
      <button style={styleDarkTheme} onClick={clickHandler}>Click me to display message</button>
    </div>
  )
}

export default EventHandling