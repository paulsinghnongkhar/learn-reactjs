import React, { Component } from 'react';
import SyntaxHighlighter from 'react-syntax-highlighter';

class Message extends Component {
    constructor() {
        super()
        this.state = {
            message: 'Welcome User',
            count: 0
        }
    }

    changeMessage() {
        this.setState({
            message: 'Thank you for subscribing',
            count: this.state.count + 1
        }, () => {
            if(this.state.count === 10) {
                this.setState({
                    message: 'Thank you for 10 subscribed please continue subscribing'
                })
            }
        })
    }

    render() {
        const codeString = 
`import React, { Component } from 'react';

class Message extends Component {
    constructor() {
        super()
        this.state = {
            message: 'Welcome User',
            count: 0
        }
    }

    changeMessage() {
        this.setState({
            message: 'Thank you for subscribing',
            count: this.state.count + 1
        }, () => {
            if(this.state.count === 10) {
                this.setState({
                    message: 'Thank you for 10 subscribed please continue subscribing'
                })
            }
        })
    }

    render() {
        return (
            <div>
                <h1>{this.state.message} - {this.state.count}</h1>
                Please <button onClick={() => this.changeMessage()}>subscribe</button>
            </div>
        )
    }
}

export default Message;`;
        return (
            <div className='content'>
                <h1>State Component</h1>
                <p>Here is the code below &#11015;&#65039;</p>
                <SyntaxHighlighter language="javascript" showLineNumbers={true}>
                    {codeString}
                </SyntaxHighlighter>
                <p>Here is the output below &#11015;&#65039;</p> 
                <h1>{this.state.message} - {this.state.count}</h1>
                <p>Please <button onClick={() => this.changeMessage()}>subscribe</button></p>
            </div>
        )
    }
}

export default Message;