import React, { useState } from "react";
import SyntaxHighlighter from 'react-syntax-highlighter';

function MapFunction() {

  let players = [
    { 
        name: "Ronaldo", 
        age: "Age: 38",
        clubs: "Clubs: United, Madrid, Juventus, Al-Nassr" 
    },
    { 
        name: "Messi", 
        age: "Age: 36",
        clubs: "Clubs: Barcelona, PSG, Inter Miami" 
    },
  ];

  let [displayPlayer, setPlayer] = useState();

  let handleNameChange = (e) => {
    setPlayer(e.target.value);
  };
  const codeString = 
  `import React, { useState } from "react";

  function MapFunction() {
  
    let players = [
      { 
          name: "Ronaldo", 
          age: "Age: 38",
          clubs: "Clubs: United, Madrid, Juventus, Al-Nassr" 
      },
      { 
          name: "Messi", 
          age: "Age: 36",
          clubs: "Clubs: Barcelona, PSG, Inter Miami" 
      },
    ];
  
    let [displayPlayer, setPlayer] = useState();
  
    let handleNameChange = (e) => {
      setPlayer(e.target.value);
    };
  
    return (
      <div className="map-component">
          <table border={1}>
              <tr>
                  <th>Player Details</th>
              </tr>
              <select onChange={handleNameChange}>
                  <option> Select Player </option>
              {
                  players.map((player) => (
                    <option value={[player.name, player.age, player.clubs]}>{player.name}</option>
                      )
                  )
              }
              </select>
              <tr>
                  <td>{displayPlayer}</td>
              </tr>
        </table>
      </div>
    );
  }
  
  export default MapFunction;`;
  return (
    <div className="content">
      <h1>map() Method Component</h1>
      <p>Here is the code below &#11015;&#65039;</p>
      <SyntaxHighlighter language="javascript" showLineNumbers={true}>
        {codeString}
      </SyntaxHighlighter>
      <p>Here is the output below &#11015;&#65039;</p> 
        <table border={1}>
            <tr>
                <th>Player Details</th>
            </tr>
            <select onChange={handleNameChange}>
                <option> Select Player </option>
            {
                players.map((player) => (
                    <option value={[player.name, player.age, player.clubs]}>{player.name}</option>
                    )
                )
            }
            </select>
            <tr>
                <td>{displayPlayer}</td>
            </tr>
      </table>
    </div>
  );
}

export default MapFunction;
