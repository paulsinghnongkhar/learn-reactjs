import React from "react";
import SyntaxHighlighter from 'react-syntax-highlighter';

function Greet() {
    const codeString = 
`import React from "react";

function Greet() 
{
    return 
    (
        <div>
            <h1>This is Functional Component</h1>
        </div>
    )
}

export default Greet;`;
    return (
        <div className="content">
            <h1>Functional Component</h1>
            <p>Here is the code below &#11015;&#65039;</p>
            <SyntaxHighlighter language="javascript" showLineNumbers={true}>
                {codeString}
            </SyntaxHighlighter>
            <p>Here is the output below &#11015;&#65039;</p> 
            <h1>This is Functional Component</h1>
        </div>
    )
}

export default Greet;