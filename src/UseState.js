import React, {useState} from 'react'
import SyntaxHighlighter from 'react-syntax-highlighter';

function UseState() {
    const [count, setCount] = useState(0)

    function decrementCount() {
        setCount(count - 1)
    }

    function incrementCount() {
        setCount(count + 1)
    }
  const codeString = 
`import React, {useState} from 'react'

function UseState() {
    const [count, setCount] = useState(0)

    function decrementCount() {
        setCount(count - 1)
    }

    function incrementCount() {
        setCount(count + 1)
    }

  return (
    <div>
      <button onClick={decrementCount}>Decreasing</button>
      <span>{count}</span>
      <button onClick={incrementCount}>Increasing</button>
    </div>
  )
}

export default UseState`;
  return (
    <div className='content'>
      <h1>useState Component</h1>
      <p>Here is the code below &#11015;&#65039;</p>
      <SyntaxHighlighter language="javascript" showLineNumbers={true}>
        {codeString}
      </SyntaxHighlighter>
      <p>Here is the output below &#11015;&#65039;</p>
      <button onClick={decrementCount}>Decreasing</button>
      <span>{count}</span>
      <button onClick={incrementCount}>Increasing</button>
    </div>
  )
}

export default UseState
