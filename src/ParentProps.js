import React, { Component } from 'react'
import SyntaxHighlighter from 'react-syntax-highlighter';
import ChildProps from './ChildProps'

class ParentProps extends Component {
  constructor(props) {
    super(props)
    this.state = {
      parentName: 'Parent Props',
    }
    this.greetParent = this.greetParent.bind(this)
  }

    greetParent(childName) {
        alert(`Hello ${this.state.parentName} greeting from ${childName}`);
    }

  render() {
    const parentString = 
`import React, { Component } from 'react'
import ChildProps from './ChildProps'

class ParentProps extends Component {
  constructor(props) {
    super(props)
    this.state = {
      parentName: 'Parent Props',
    }
    this.greetParent = this.greetParent.bind(this)
  }

    greetParent(childName) {
      alert('Hello ${`this.state.parentName`} greeting from ${`childName`}');
    }

  render() {
    return (
      <div>
        <ChildProps greetHandler={this.greetParent} />
      </div>
    )
  }
}

export default ParentProps`;
const childString = 
`import React from 'react'

function ChildProps(props) {
  return (
    <div>
      <button onClick={() => props.greetHandler('Child Props')}>Greet Parent</button>
    </div>
  )
}

export default ChildProps`;
    return (
      <div className='content'>
        <h1>Props Component</h1>
        <p>Here below is the code of ParentProps.js component &#11015;&#65039;</p>
        <SyntaxHighlighter language="javascript" showLineNumbers={true}>
          {parentString}
        </SyntaxHighlighter>
        <p>Here below is the code of ChildProps.js component &#11015;&#65039;</p>
        <SyntaxHighlighter language="javascript" showLineNumbers={true}>
          {childString}
        </SyntaxHighlighter>
        <p>Here is the output below &#11015;&#65039;</p> 
        <ChildProps greetHandler={this.greetParent} />
      </div>
    )
  }
}

export default ParentProps