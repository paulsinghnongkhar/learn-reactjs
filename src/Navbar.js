import React from 'react'
import { Link } from 'react-router-dom';
import Logo from './image/science.png';

const Navbar = () => {

    return (  
        <nav className="navbar">
            <h1>Learn Basic <img src={Logo} alt=''></img>ReactJS</h1>
            <div className="links">
                <Link to="/" className="link">Introduction</Link>
                <Link to="/functional&class" className="link">Functional & Class Component</Link>
                <Link to="/props&state" className="link">Props & State</Link>
                <Link to="/eventhandling" className="link">Event Handling</Link>
                <Link to="/mapmethod&filtermethod" className="link">map() & filter() Method</Link>
                <div className='hooks'>
                    <h1><img src={Logo} alt='' />React Hooks</h1>
                    <Link to="/usestate&useeffect" className="link">1. useState & useEffect</Link>
                    <Link to="/usecontext" className="link">2. useContext</Link>
                </div>
            </div>
        </nav>
    );
}
 
export default Navbar;