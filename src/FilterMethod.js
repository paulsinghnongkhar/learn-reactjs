import React from 'react'
import SyntaxHighlighter from 'react-syntax-highlighter';

function FilterMethod() {

    const ages = [20, 22, 24, 26, 28];

    function checkAge(age) {
        return age < document.getElementById("ageToCheck").value;
    }

    function myFunction() {
        document.getElementById("demo").innerHTML = ages.filter(checkAge);
      }
      const codeString = 
`import React from 'react'

function FilterMethod() {

    const ages = [20, 22, 24, 26, 28];

    function checkAge(age) {
        return age < document.getElementById("ageToCheck").value;
    }

    function myFunction() {
        document.getElementById("demo").innerHTML = ages.filter(checkAge);
      }

  return (
    <div>
    <p>filter this age 20, 22, 24, 26, 28</p>
      <input type="number" id="ageToCheck" />
      <button onClick={myFunction}>Test</button>
      <p id="demo"></p>
    </div>
  )
}

export default FilterMethod`;
  return (
    <div className='content'>
                    <h1>filter() Methdod</h1>
            <p>Here is the code below &#11015;&#65039;</p>
            <SyntaxHighlighter language="javascript" showLineNumbers={true}>
                {codeString}
            </SyntaxHighlighter>
            <p>Here is the output below &#11015;&#65039;</p> 
    <p>filter these age 20, 22, 24, 26, 28</p>
      <input type="number" id="ageToCheck" />
      <button onClick={myFunction}>Filter age</button>
      <p id="demo"></p>
    </div>
  )
}

export default FilterMethod
