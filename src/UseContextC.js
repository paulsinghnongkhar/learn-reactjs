import React, {useContext} from 'react'
import { UserContext, TitleContext } from './App'

function UseContextC() {

    const user = useContext(UserContext)
    const title = useContext(TitleContext)

  return (
    <div className='content'>
        {user} {title}
    </div>
  )
}

export default UseContextC
