import React, {useEffect, useState} from 'react'
import SyntaxHighlighter from 'react-syntax-highlighter';

function Useeffect() {
const [second, setSecond] = useState(9)
const [minute, setMinute] = useState(0)
const [hour, setHour] = useState(0)

    useEffect(() => {
    const myTime1 = setTimeout(() =>{
        setSecond(second - 1)
      }, 1000)
      if(second === 0) {
        const myTime2 = setTimeout(() =>{
          setSecond(second + 9)
          setMinute(minute + 1)
        }, 1000)
        if(minute === 9) {
        const myTime3 = setTimeout(setMinute(minute === 0))
        const myTime4 = setTimeout(() => {
            setHour(hour + 1)
          }, 1000)
          if(hour === 3) {
            clearTimeout(myTime4)
            clearTimeout(myTime3)
          }
        }
        if(hour === 3) {
          clearTimeout(myTime1)
          clearTimeout(myTime2)
        }
      }
    })

  const codeString = 
  `import React, {useEffect, useState} from 'react'

  function Useeffect() {
  const [second, setSecond] = useState(60)
  
      useEffect(() => {
        const myTime = setTimeout(() =>{
          setSecond(second - 1)
        }, 1000)
        if(second === 0) {
          clearTimeout(myTime)
          document.getElementById("message").innerHTML = "Time Up";
        }
      })
  
    return (
      <div>
        <p>Time start now</p>
        <h1>Time: {second}s</h1>
        <p id='message'></p>
      </div>
  
    )
  }
  
  export default Useeffect`;
  return (
    <div className='content'>
      <h1>useEffect Component</h1>
      <p>Here is the code below &#11015;&#65039;</p>
      <SyntaxHighlighter language="javascript" showLineNumbers={true}>
        {codeString}
      </SyntaxHighlighter>
      <p>Here is the output below &#11015;&#65039;</p> 
      <p>This is counting h-hour m-minute and s-second</p>
      <h1>{hour}h {minute}m {second}s</h1>
      <p id='message'></p>
    </div>

  )
}

export default Useeffect
