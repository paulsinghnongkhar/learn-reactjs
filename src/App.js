import React from 'react'
import Navbar from './Navbar';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import FunctionalComp from './FunctionalComp';
import ClassComp from './ClassComp';
import ParentProps from './ParentProps';
import State from './State';
import EventHandling from './EventHandling';
import Introduction from './Introduction';
import UseState from './UseState';
import UseEffect from './UseEffect';
import MapFunction from './MapFunction';
import FilterMethod from './FilterMethod';
import UseContextA from './UseContextA';

export const UserContext = React.createContext()
export const TitleContext = React.createContext()

function App() {
  return (
    <Router>
      <div className="App">
        <Navbar />
          <div className="content">
            <Switch>
            <Route exact path="/">
              <Introduction />
            </Route>
            <Route path="/functional&class">
              <FunctionalComp />
              <ClassComp />
            </Route>
            <Route path="/props&state">
              <ParentProps />
              <State />
            </Route>
            <Route path="/eventhandling">
              <EventHandling />
            </Route>
            <Route path="/usestate&useeffect">
              <UseState />
              <UseEffect />
            </Route>
            <Route path="/mapmethod&filtermethod">
              <MapFunction />
              <FilterMethod />
            </Route>
            <Route path="/usecontext">
              <UserContext.Provider value={'Paulsingh'}>
                <TitleContext.Provider value={'Nongkhar'}>
                  <UseContextA />
                </TitleContext.Provider>
              </UserContext.Provider>
            </Route>
            </Switch>
          </div>
      </div>
    </Router>

  );
}
export default App;