import React, { useContext } from 'react'
import UseContextC from './UseContextC'
import { UserContext, TitleContext } from './App'

function UseContextB() {

    const user = useContext(UserContext)
    const title = useContext(TitleContext)

  return (
    <div className='content'>
      {user} {title}
    </div>
  )
}

export default UseContextB
