import React, { Component } from 'react';
import SyntaxHighlighter from 'react-syntax-highlighter';

class Welcome extends Component {
    render() {
const codeString = 
`import React, { Component } from 'react';

class Welcome extends Component {
    render() 
    {
        return 
        (
            <div>
                <h1>This is Class Component</h1>
            </div>
        )
    }
}

export default Welcome;`;
        return (
            <div className='content'>
            <h1>Class Component</h1>
            <p>Here is the code below &#11015;&#65039;</p>
            <SyntaxHighlighter language="javascript" showLineNumbers={true}>
                {codeString}
            </SyntaxHighlighter>
            <p>Here is the output below &#11015;&#65039;</p> 
                <h1>This is Class Component</h1>
            </div>
        )
    }
}

export default Welcome;